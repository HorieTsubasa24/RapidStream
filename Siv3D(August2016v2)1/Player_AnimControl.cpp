#include "Player_AnimControl.h"

void Player_AnimControl::GoForward(Player_Input& pi)
{
	vel.x += forwardForce * pi.axis.x;
}

void Player_AnimControl::GoBackward(Player_Input& pi)
{
	vel.x += forwardForce * pi.axis.x;
}

void Player_AnimControl::GoJump()
{
	if (pos.y > 0)
		return;

	isjumped = true;
	vel.y = forwardForce * 4;
	anim->play("Idle/jump", 1);
	toIdleWait = 0;
	SoundAsset(L"Hit2").play();
}

void Player_AnimControl::GoAttack()
{
	if (toIdleWait > 9)
		return;

	anim->play("Idle/Idle_attack", 1);
	toIdleWait = TOIDLEWAIT;
	toWaitForAttack = TOWAITFORATTACK;
	SoundAsset(L"Hit2").play();
}

void Player_AnimControl::GoThrow(array<CharaParam, 90>& cp, CharacterDatas* cd)
{
	if (toIdleWait > 9)
		return;

	anim->play("Idle/run_attack", 1);
	toIdleWait = TOIDLEWAIT;
	toWaitForAttack = TOWAITFORATTACK;
	SoundAsset(L"Hit2").play(); 
	ToAppearKunai(cp, cd);
}

void Player_AnimControl::ToAppearKunai(array<CharaParam, 90>& cp, CharacterDatas* cd)
{
	for (int i = 0; i < cp.size(); i++)
	{
		if (cp[i].CharaId == 0)
		{
			cp[i] = cd->GetData(1);
			auto v = basepos;
			v.x += 60.0f + pos.x;
			v.y -= pos.y + 40.0f;
			cp[i].player->setPosition(v.x, v.y);
			cp[i].SetPosVec(v);
			break;
		}
	}

}

void Player_AnimControl::PhysicalOperation()
{
	int sign = Math::Sign(vel.x);

	// 横の減速。
	vel.x += - sign * resistanceVelocity;
	if (Math::Abs(vel.x) < 0.2f)
		vel.x = 0;
	
	if (Math::Abs(vel.x) > forwardForce)
		vel.x = sign * forwardForce;

	// 縦の重力演算
	if (pos.y > 0) {
		vel.y -= gravity;
	}

	// 座標演算
	pos.x += vel.x;
	pos.y += vel.y;

	// ジャンプ解除
	if (pos.y < 0.0f) {
		pos.y = 0;
		vel.y = 0;
	}
}

// "Idle", "Idle_attack", "jump", "jump_efk", "run", "run_attack"
void Player_AnimControl::PlayerMove(Player_Input& pi, array<CharaParam, 90>& cp, CharacterDatas* cd)
{
	// 攻撃間ウェイト
	if (toWaitForAttack > 0) {
		// 苦無攻撃はキャンセル可能
		if (anim->getPlayAnimeName() == "run_attack" && !pi.buttons[2]) {
			toWaitForAttack = 0;
			return;
		}
		toWaitForAttack--;
	}

	// 前後移動
	if (pi.axis.x > 0.2f && pos.x < 1120.0f)
		GoForward(pi);
	else if (pi.axis.x < -0.2f && pos.x > 30.0f)
		GoBackward(pi);

	// ジャンプ
	if (pi.buttons[0] || pi.buttons[3])
		GoJump();		//isjumped = true
	else if (pos.y > 50 && isjumped == true) {
		// ジャンプキャンセル。
		isjumped = false;
		if (vel.y > gravity)
			vel.y = 3 * gravity;
	}

	// 攻撃ウェイト0
	if (toWaitForAttack == 0) {
		// 刀攻撃
		if (pi.buttons[1])
			GoAttack();

		// 苦無攻撃
		if (pi.buttons[2])
			GoThrow(cp, cd);
	}

	PhysicalOperation();
	AnimUpdate();
}

void Player_AnimControl::AnimUpdate() {
	if (toIdleWait > 0)
		toIdleWait--;

	if (pos.y == 0 && toIdleWait == 0) {
		anim->play("Idle/run", 0);
		toIdleWait = -1;
	}

	anim->setPosition(basepos.x + pos.x, basepos.y - pos.y);
}

void Player_AnimControl::SetCharacter(ss::Player* pl) {
	anim = pl;
	anim->play("Idle/run", 0);
}

void Player_AnimControl::init() {
	anim = nullptr;
	pos = Vec2(0, 0);
	vel = Vec2(0, 0);
	toIdleWait = 0;
	isjumped = false;
}

Vec2 Player_AnimControl::GetposVec()
{
	return (pos + basepos);
}

Player_AnimControl::Player_AnimControl()
{
}


Player_AnimControl::~Player_AnimControl()
{
}
