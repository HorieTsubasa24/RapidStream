#pragma once
#include "Array_temp.h"
#include <Siv3D.hpp>

using namespace std;

/// <summary>
/// キャラクターデータのマスターデータ的な役割。
/// Characterクラスから初期化時に呼び出されては消える。
/// </summary>
class CharacterAttribute
{
private:
	/// <summary>
	/// キャラの名前
	/// </summary>
	string chara_Name;

	/// <summary>
	/// プロジェクト内の各アニメーションの名前はここで定義される。
	/// </summary>
	vector<string> anime_State;

	int alpha;
	float scale;
	float chara_speed;
	float chara_jump;
	Vec2 acs = Vec2(0, -1.0f);			// 基準値の例
	Vec2 pos = Vec2(3.0f, -4.0f);	// 基準値の例
	void SetParam(string name, vector<string> anims, float spd, float jmp, int alp, float scl, Vec2 ac, Vec2 mvel);
public:
	int GetAlpha();
	float GetScale();
	Vec2 GetAcs();
	Vec2 GetPos();

	CharacterAttribute();

	/// <summary>
	/// プレイヤー、敵キャラの初期設定。
	/// インデックスごとにキャラを返す。
	/// </summary>
	/// <param name="gimic_num"></param>
	CharacterAttribute(int chara_num);
	~CharacterAttribute();
};

