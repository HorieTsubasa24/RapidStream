#include "Score.h"

long Score::scores[5] = { 15000, 12000, 9000, 6000, 3000 };

void Score::init()
{
	isFadeComp = false;
	fade.init();
	isFirst = true;
	isgototitle = false;
}

void Score::ShowHiScore()
{
	for (int i = 0; i < 5; i++) {
		font(ToString(scores[i])).draw(510, 202 + i * 107, colorTone);
	}
}

void Score::SetHiScore()
{
	auto scr = Game::score;
	for (int i = 0; i < 5; i++) {
		if (scr > scores[i]) {
			for (int j = 4; j > i; j--) {
				scores[j] = scores[j - 1];
			}
			scores[i] = scr;
			return;
		}
	}
}

void Score::firstScene() {
	if (isFirst) {
		PlayerMemory::ToFreeMemory();
		SetHiScore();
		isFirst = false;
	}
}

void Score::sceneDraw() {
	TextureAsset(L"ScoreBG").draw(0, 0, colorTone); 
	ShowHiScore();
}

Scene Score::Run()
{
	Scene sc = Scene::SCORE;

	// フェードイン
	bool fadeincomp = Fadein();

	// フェードアウトしてシーン移行
	sc = Fadeout(isgototitle, sc, Scene::TITLE);

	// 初期化の次の処理
	firstScene();

	input = play_cont.GetPlayerInput();
	sceneDraw();

	// 決定ボタンでシーン移行開始
	if (isgototitle == false && fadeincomp == true && isgototitle == false && input.buttons[0] == true) {
		fade.init();
		SoundAsset(L"Decision").play();
		isgototitle = true;
	}

	if (sc != Scene::SCORE)
		init();

	return sc;
}

Score::Score(GameSound& snd, GimicDatas& g_data, CharacterDatas& c_data)
{
	InitReference(snd, g_data, c_data);
	init();
}


Score::~Score()
{
	SoundAsset(L"Ep1");
}
