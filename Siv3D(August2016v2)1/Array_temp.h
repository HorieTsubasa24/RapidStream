#pragma once
#include <cstdlib>
#include "Array_temp.h"

template
<
	typename TYPE,
	std::size_t SIZE
>
std::size_t array_length(const TYPE(&)[SIZE])
{
	return SIZE;
}
