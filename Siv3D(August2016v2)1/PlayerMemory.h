#pragma once
#include <Siv3D.hpp>
#include "ssbpLib/SS5Player.h"
#include "CharaParam.h"

/// <summary>
/// コピーインスタンス追加、開放、操作は全てここから行う。
/// </summary>
static class PlayerMemory
{
private:
public:
	/// <summary>
	/// コピーインスタンスの参照リスト
	/// </summary>
	static std::vector<ss::Player*> memory;

	/// <summary>
	/// 自機への参照。
	/// </summary>
	static ss::Player* player;

	/// <summary>
	/// コピーインスタンスに参照を追加する。ポインタを直接代入。
	/// </summary>
	/// <param name="p"></param>
	static void dataPushptr(ss::Player* p);

	/// <summary>
	/// コピーインスタンスに参照を追加する。CharaParam.playerを代入。
	/// </summary>
	/// <param name="cp"></param>
	static void dataPush(CharaParam& cp);

	/// <summary>
	/// プレイヤーのときはこっち。
	/// </summary>
	/// <param name="p"></param>
	static void dataPushptrPlayer(ss::Player* p);

	/// <summary>
	/// 苦無のときはこっち。
	/// </summary>
	/// <param name="p"></param>
	static void dataPushptrKunai(ss::Player* p);

	/// <summary>
	/// コピーインスタンスに参照を追加する。CharaParam.playerを代入。
	/// </summary>
	/// <param name="cp"></param>
	static void dataPushPlayer(CharaParam& cp);

	/// <summary>
	/// コピーインスタンスに参照を追加する。CharaParam.kunaiを代入。
	/// </summary>
	/// <param name="cp"></param>
	static void dataPushKunai(CharaParam& cp);

	/// <summary>
	/// update毎に外部から呼び出す。
	/// プレイヤーの振る舞いを更新させる。
	/// </summary>
	static void ShowCharacter(Color& col);

	/// <summary>
	/// 苦無のデータをplayerのポインタがnullなら削除。
	/// </summary>
	static void KunaiDataCheckAndDel(CharaParam& cp);

	/// <summary>
	/// 指定したポインタのキャラクターを削除。
	/// 指定したポインタのキャラクターがなければ何もしない。
	/// </summary>
	static void DeleteCharacter(ss::Player* pl);

	/// <summary>
	/// キャラパラメータから削除。
	/// </summary>
	/// <param name="cp"></param>
	static void CharaDeleteFromCP(CharaParam& cp);

	/// <summary>
	/// コピーインスタンスの開放
	/// </summary>
	static void ToFreeMemory();

	PlayerMemory() {};
	~PlayerMemory();
};

