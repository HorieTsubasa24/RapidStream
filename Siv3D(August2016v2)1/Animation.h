#pragma once
#include "ssbpLib/SS5Player.h"
#include "Array_temp.h"

/// <summary>
/// CharacterDatasクラスからデータを引っ張って使う。
/// </summary>
class Animation
{
private:
	static const int rsrc_Num = 9;
	std::string resourceString[rsrc_Num] = {"Player\\NewProject.ssbp", "Title\\splash512.ssbp", "TsSoft\\TsSoft.ssbp", "Yane\\Yane.ssbp", "TitleLogo\\RapidStream.ssbp", "Mash\\Mash.ssbp",
											"Kunai\\Kunai.ssbp", "Nyan\\Nyan.ssbp", "Mafies\\Mafies.ssbp"};
	Animation(const Animation& src) = delete;
public:
	/// <summary>
	/// アニメーションのリソース登録場所。
	/// </summary>
	ss::ResourceManager *resman;// = new ss::ResourceManager[m];

	/// <summary>
	/// プレイヤーキャラのアニメーション。
	/// </summary>
	ss::Player *ssplayer;// = new ss::Player[n];

	Animation();
	~Animation();
};

