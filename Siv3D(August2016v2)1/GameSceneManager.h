#pragma once
#include "GameSound.h"
#include "CharacterDatas.h"
#include "GimicDatas.h"
#include "GameSound.h"
#include "Scene.h"
#include "Fade.h"
#include "BGChara.h"
#include "Player_Control.h"

/// <summary>
/// 各シーンのデータソースセットと共通メソッド。
/// </summary>
class GameSceneManager
{
protected:
	/// <summary>
	/// firstScene()が実行されたか。
	/// </summary>
	bool isFirst = true;

	/// <summary>
	/// フェードインが完了しているか。
	/// </summary>
	bool isFadeComp = false;

	/// <summary>
	/// フェード遷移をする。
	/// </summary>
	Fade fade;
	
	/// <summary>
	/// 背景の色調。
	/// </summary>
	Color colorTone = Color(0, 0, 0);

	/// <summary>
	/// 入力を見る。
	/// </summary>
	Player_Control play_cont;
	
	/// <summary>
	/// 入力の結果。
	/// </summary>
	Player_Input input;
public:
	virtual Scene Run() = 0;
	/// <summary>
	/// シーンの初期化。シーン作成時とシーン変更時に呼び出す。
	/// </summary>
	virtual void init();

	/// <summary>
	/// シーンに入って初めに実行するメソッド。
	/// </summary>
	virtual void firstScene() = 0;

	/// <summary>
	/// シーンの要素の描画。
	/// </summary>
	virtual void sceneDraw() = 0;

	/// <summary>
	/// フェードイン。
	/// </summary>
	bool Fadein();

	/// <summary>
	/// フェードアウト。
	/// </summary>
	Scene Fadeout(bool isGotoFadeout, Scene nowsce, Scene gosce);

	GameSound* sounds;
	GimicDatas* gimics;
	CharacterDatas* charas;
	/// <summary>
	/// 背景表示をする。
	/// </summary>
	BGChara* bg_chara;

	/// <summary>
	/// シーンを作成する時、リソースの参照を登録する。
	/// </summary>
	/// <param name="snd"></param>
	/// <param name="g_data"></param>
	/// <param name="c_data"></param>
	void InitReference(GameSound& snd, GimicDatas& g_data, CharacterDatas& c_data, BGChara& b_chr);

	/// <summary>
	/// シーンを作成する時、リソースの参照を登録する。
	/// </summary>
	/// <param name="snd"></param>
	/// <param name="g_data"></param>
	/// <param name="c_data"></param>
	void InitReference(GameSound& snd, GimicDatas& g_data, CharacterDatas& c_data);
	GameSceneManager();
	virtual ~GameSceneManager();
};

