#pragma once
#include "GameSceneManager.h"

class Lunch : GameSceneManager
{
private:
	// override
	void init();
	void sceneDraw();
	void firstScene();

	const int FLAME_COUNT = 132;
	int firstCount = FLAME_COUNT;
	int secondCount = FLAME_COUNT;

	void ShowSpriteStudio();
	void ShowTsSoft();
public:
	Scene Run();
	Lunch(GameSound& snd, GimicDatas& g_data, CharacterDatas& c_data);
	~Lunch();
};

