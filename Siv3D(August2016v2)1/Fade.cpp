#include "Fade.h"

void Fade::init() {
	tim = TIM;
	bool isFirst = true;
}

Scene Fade::Fadeout(Color& cl, Scene nowsc, Scene gotosc)
{
	if (isFirst == true) {
		tim = TIM;
		cl = Color(255, 255, 255);
		isFirst = false;
		return nowsc;
	}

	const float c = (255 / TIM);
	cl.r -= c;
	cl.g -= c;
	cl.b -= c;

	tim--;
	if (tim == 0)
		return gotosc;

	return nowsc;
}


bool Fade::Fadein(Color& cl)
{
	if (isFirst == true) {
		tim = TIM;
		cl = Color(0, 0, 0);
		isFirst = false;
		return false;
	}

	const float c = (255 / TIM);
	cl.r += c;
	cl.g += c;
	cl.b += c;

	tim--;
	if (tim == 0)
		return true;

	return false;
}


Fade::Fade()
{
	init();
}


Fade::~Fade()
{
}
