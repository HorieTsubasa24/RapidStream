#pragma once
#include <Siv3D.hpp>

class BGChara
{
private:
	/// <summary>
	/// 背景の数。
	/// </summary>
	static const int sp_Num = 6;

	/// <summary>
	/// 一度のルーチンのスクロール回数。
	/// </summary>
	static const int sp_ScrollNum = 5;

	/// <summary>
	/// 背景情報。全部で3つ。
	/// </summary>
	struct BG {
		/// <summary>
		/// 色変更する。
		/// </summary>
		bool isChangeColor = true;

		/// <summary>
		/// 背景テクスチャ。
		/// </summary>
		Texture tex;

		/// <summary>
		/// スクロールするスピード
		/// </summary>
		double spd;

		/// <summary>
		/// 描画位置。左上がトップ。
		/// </summary>
		Vec2 pos = Vec2(0, 0);

		/// <summary>
		/// 背景の色。色々変化起こしたい。
		/// </summary>
		ColorF color = Color(1.0f, 1.0f, 1.0f, 1.0f);

		/// <summary>
		/// 色の位相。spdによって変化する速度が変わる。
		/// </summary>
		Vec3 colorPhese = Vec3(0.0f, 0.5f, 1.0f);
	};

	/// <summary>
	/// BGスプライトの情報。
	/// </summary>
	BG bg_sp[sp_Num];

	/// <summary>
	/// 色相を進ませる。
	/// </summary>
	void advPhese(BG& bg);

	/// <summary>
	/// 背景をスクロールさせる。
	/// </summary>
	void advPos(BG& bg);
public:
	/// <summary>
	/// 背景表示。
	/// </summary>
	/// <returns></returns>
	void showBG(bool isscroll, Color& col);

	BGChara();
	~BGChara();
};

