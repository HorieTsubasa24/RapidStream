#pragma once
#include <Siv3D.hpp>

struct Player_Input {
	std::vector<bool> buttons;
	Vec2 axis;

	Player_Input(std::vector<bool> btn, Vec2 axs) {
		buttons = btn;
		axis = axs;
	}
	Player_Input() {
		buttons = vector<bool>();
		axis = Vec2(0, 0);
	}
};
