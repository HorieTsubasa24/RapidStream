#include "BGChara.h"

void BGChara::advPhese(BG& bg)
{
	for (int i = 0; i < BGChara::sp_ScrollNum; i++) {
		bg.pos.x -= bg.spd;
		if (bg.pos.x < -1279.9f)
			bg.pos.x = 1280.0f;
	}
}

void BGChara::advPos(BG& bg)
{
	if (bg.isChangeColor == false) {
		bg.color.a = 1.0f;
		return;
	}

	bg.colorPhese = bg.colorPhese + bg.spd * 0.1f * Vec3(0.01f, 0.02f, 0.04f);
	if (bg.colorPhese.x >= Math::Pi)
		bg.colorPhese.x = 0;
	if (bg.colorPhese.y >= Math::Pi)
		bg.colorPhese.y = 0;
	if (bg.colorPhese.z >= Math::Pi)
		bg.colorPhese.z = 0;

	bg.color.r = Math::Sin(bg.colorPhese.x) / 2 + 0.50f;
	bg.color.g = Math::Sin(bg.colorPhese.y) / 2 + 0.50f;
	bg.color.b = Math::Sin(bg.colorPhese.z) / 2 + 0.50f;
	bg.color.a = 1.0f;
}

void BGChara::showBG(bool isscroll, Color& col)
{
	for (int i = 0; i < sp_Num; i++) {
		bg_sp[i].tex.draw(bg_sp[i].pos.x, bg_sp[i].pos.y, bg_sp[i].color * col);
		if (isscroll == false)
			continue;

		advPos(bg_sp[i]);
		advPhese(bg_sp[i]);
	}
}

BGChara::BGChara()
{
	s3d::String file[sp_Num] = { L"BG/bg0.png", L"BG/bg0.png", L"BG/bg1.png", L"BG/bg1.png", L"BG/bg2.png", L"BG/bg2.png" };
	double spd[sp_Num] = { 1.0f, 1.0f, 2.0f, 2.0f, 4.0f, 4.0f };
	bool is_colchange[sp_Num] = { true, true, true, true, false, false };

	for (int i = 0; i < sp_Num; i++) {
		Image img = Image(file[i]).scale(1280, 720);
		bg_sp[i].tex = Texture(img);
		bg_sp[i].pos = Vec2(1280 * (i % 2), 0);
		bg_sp[i].spd = spd[i];
		bg_sp[i].color = ColorF(1.0f, 1.0f, 1.0f, 1.0f);
		bg_sp[i].isChangeColor = is_colchange[i];
	}
}


BGChara::~BGChara()
{
}
