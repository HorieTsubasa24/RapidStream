#pragma once
#include <Siv3D.hpp>
#include "CharaParam.h"
#include "Particle.h"

static class EffectMemory
{
public:
	/// <summary>
	/// エフェクトのインスタンス
	/// </summary>
	static Effect* effects;

	/// <summary>
	/// 爆発エフェクト。
	/// </summary>
	/// <param name="pos"></param>
	static void addBombEffect(Vec2& pos);

	/// <summary>
	/// ダメージエフェクト。
	/// </summary>
	/// <param name="pos"></param>
	static void addDamageEffect(Vec2& pos);

	static void update();

	EffectMemory();
	~EffectMemory();
};

