#pragma once
#include <Siv3D.hpp>

struct Bomb : IEffect
{
	struct Particle
	{
		Vec2 pos;
		Vec2 vel;
	};

	Array<Particle> m_particles;

	Bomb(const Vec2& pos, int count)
		: m_particles(count)
	{
		for (auto& particle : m_particles)
		{
			const Vec2 v = Circular(4.0f, Random(TwoPi));
			particle.pos = pos + v;
			double x = Random(100.0f, 500.0f) * (Random() <= 0.5f ? -1 : 1);
			double y = Random(100.0f, 500.0f) * (Random() <= 0.5f ? -1 : 1);
			particle.vel = Vec2(x, y);
		}
	}

	bool update(double t) override
	{
		for (const auto& particle : m_particles)
		{
			const Vec2 pos = particle.pos + particle.vel * t;

			Circle(pos, 18).draw(HSV(pos.y / 4.0, 0.6, 1.0).toColorF(1.0 - t));
		}

		return t < 1.0;
	}
};